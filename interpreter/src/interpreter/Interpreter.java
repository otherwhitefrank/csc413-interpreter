/*
 The interpreter parses the command line and passes it to the ByteCodeLoader, it then executes the returned
Program object line for the line, managing the flow of execution and the RunTimeStack
 */

package interpreter;

import java.io.*;

/**
 * Interpreter run the interpreter:
 * 1. Perform all initialization
 * 2. Load the bytecodes from file
 * 3. Run the virtual machine
 * 
 * @author Frank Dye
 */
public class Interpreter
{
    ByteCodeLoader bcl;
    
    /**
     * Open a ByteCodeLoader and gets the CodeTable ready for use
     * @param codeFile 
     */
    public Interpreter(String codeFile) {
        try {
            CodeTable.init();
            bcl = new ByteCodeLoader(codeFile);
        }
        catch (IOException e)
        {
            System.out.println("***IOException cannot find file: " + codeFile);
            System.exit(1);
        }
    }
    
    /**
     * Loads codes from ByteCodeLoader into Program object, then initializes 
     * the vm and begins execution.
     */
    void run()
    {
        Program program = bcl.loadCodes();
        VirtualMachine vm = new VirtualMachine(program);
        vm.executeProgram();
    }
    
    /**
     * Takes as input the file to open, creates new Interpreter object and executes run()
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        if (args.length == 0)
        {
            System.out.println("***Incorrect usage, try: java interpreter.Interpreter <file>");
            System.exit(1);
        }
        (new Interpreter(args[0])).run();
    }
    
}
