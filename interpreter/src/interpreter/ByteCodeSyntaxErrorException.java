//Simple SyntaxErrorException for use in the ByteCode class

package interpreter;

/**
 *  General purpose exception used by Interpreter, Program, ByteCode and its children, VM, and RunStack
 * @author Frank Dye
 */
public class ByteCodeSyntaxErrorException extends Exception {

    /**
     *
     * @param msg
     */
    public ByteCodeSyntaxErrorException(String msg) {
        super(msg);
    }
    
}
