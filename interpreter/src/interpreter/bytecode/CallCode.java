package interpreter.bytecode;

import interpreter.VirtualMachine;
import java.util.List;
import java.util.Vector;

/**
 * CALL ByteCode, takes one argument
 * @author frank
 */
public class CallCode extends ByteCode {

    private String jumpAddr; //Address for call to jump to

    public CallCode() {
        super("CallCode");
    }
    
    /**
     * Branches to first arg's address. Program takes care of address resolution
     * prior to execution.
     *
     * @param vm
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Save the address to return to
        vm.pushReturnAddress();
        //Jump to the new address
        vm.setProgramCounter(Integer.parseInt(this.jumpAddr));
    }

    public String getAddress()
    {
        return this.jumpAddr;
    }
    
    public void setAddress(String addr)
    {
        this.jumpAddr = addr;
    }
    
    /**
     * Special Debug Print, form "CALL f<<3>>   f(2,3)"
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        String id = this.getSourceByteCode();
        List argList = vm.getArgs();
        
        //Kinda a messy way to get the function name
        //I have to do this since the first argument is the resolved address
        //not the original line "fib<<2>>". This means I have to go back
        //to the original source code, split it by spaces
        //then split it again by "<<"
        String splitID[] = id.split("\\s");
        String id2 = splitID[1];
        
        String splitID2[] = id2.split("<<");
        String funcID = splitID2[0];
        
        String formattedString = this.getSourceByteCode()
                + "\t" + funcID + "(";
        
        for (int i = 0; i < argList.size(); i++)
        {
            //Append the argument
            formattedString += argList.get(i);
            //If this isn't the last argument append a ","
            if (i != (argList.size() - 1))
            {
                formattedString += ",";
            }
        }
        
        formattedString += ")";
        
        return formattedString;
    }

    @Override
    public void init(Vector<String> args) {
    
        if (!args.isEmpty())
        {
            //Parse the first argument as our jump address
            this.jumpAddr = args.get(0);
        }
        
    }
}
