package interpreter.bytecode;

import interpreter.VirtualMachine;
import java.util.Vector;

/**
 * STORE ByteCode, maximum of 2 arguments
 * @author frank
 */
public class StoreCode extends ByteCode {

    private int offset; //Offset to store top of stack at
    private String comment; //Optional comment to store
    private int numArgs; //Number of arguments in this ByteCode
    
    public StoreCode() {
        super("StoreCode");
    }

    /**
     * Pop the top of the stack Store the value frame + offset spots down the
     * runStack
     *
     * @param vm
     */
    @Override
    public void execute(VirtualMachine vm) {
        //Tell the vm to store top of stack at offset
        vm.storeStackOffset(this.offset);
    }

    /**
     * Special Debug output "STORE 1 k       k = 3"
     * @param vm 
     */
    @Override
    public String getDebugLine(VirtualMachine vm) {
        //If no ID present then print standard sourceByteCode
        //If there is then print "STORE 1 k     k = 3"
        if (this.numArgs == 1)
        {
            //In case "LOAD 0" simply print the original sourceByteCode
            return this.getSourceByteCode();
        }
        else
        {
            //Case "STORE 1 k       k = 3" 
            int topOfStack = vm.peekStack();
            String formattedString = this.getSourceByteCode()
                    + "\t" + this.comment + " = " + topOfStack;
            return formattedString;
        }
    }

    @Override
    public void init(Vector<String> args) {
        if (!args.isEmpty())
        {
            //Store offset and optional comment
            this.offset = Integer.parseInt(args.get(0));
            if (args.size() == 2)
            {
                this.comment = args.get(1);
            }
            
            this.numArgs = args.size();
        }
    }
}
