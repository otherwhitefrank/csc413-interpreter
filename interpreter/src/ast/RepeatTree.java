/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast;

import visitor.ASTVisitor;

/**
 *
 * @author Frank Dye
 */
public class RepeatTree extends AST
{

    public RepeatTree()
    {
    }

    public Object accept(ASTVisitor v)
    {
        return v.visitRepeatTree(this);
    }

}
